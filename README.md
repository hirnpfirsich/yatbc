# YATBC - Yet Another Telegram Bot Client

YATBC is a very simple bash script that is able to send telegram messages via the telegram bot api.

---

# DISCLAIMER
**This is not an endorsement for telegram what so ever. Please decide for yourself if telegram is a good choice for you.**

---

# Features

* send telegram messages
* splits up the message text into multiple telegram messages if there are too long
* nothing else :)

# Requirements

* `bash`
* `curl`

# Usage

```sh
echo "Test Message" | ./telegram -b <bot_token> -c <chat_id>
```

The script reads the message to send via stdin.

For more information regarding the setup of a bot please see the [Telegram Bot API Documentation](https://core.telegram.org/bots/api).

# Disclaimers

## Security

This script leaks the bot token, chat id and message to the process list of your os.

## Error Handling

This script does little to no error handling. If curl decides that the return from the telegram bot api is bad the return code for this script will be `!=0`
