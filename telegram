#!/bin/sh

set -e

VERSION=1
TEXT_LIMIT=4096
API_BASE_URL=https://api.telegram.org/bot

usage() {
	echo "version: ${VERSION}"
	echo "usage: $0 -b <bot_token> -c <chat_id> [-v|-h]"
	echo "    send telegram message to <chat id> via <bot_token>"
	echo
	echo "    reads text to send from stdin"
	echo "    if message is longer than ${TEXT_LIMIT} characters (telegram limit) text will be split into multiple messages"
	echo
	echo "    -v|-h: display this message"
	exit 1
}

sendMessage() {
	bot_token="${1}"
	chat_id="${2}"
	text="${3}"

	api_url="${API_BASE_URL}${bot_token}/sendMessage"

	if [ "${#text}" -le "$TEXT_LIMIT" ]; then
		curl -sSf --data-urlencode "chat_id=${chat_id}" --data-urlencode text="${text}" "${api_url}" > /dev/null
	else
		curl -sSf --data-urlencode "chat_id=${chat_id}" --data-urlencode text="${text:0:$TEXT_LIMIT}" "${api_url}" > /dev/null
		sendMessage "${chat_id}" "${text:$TEXT_LIMIT}"
	fi
}

# parse cli params
while getopts 'b:c:' option; do
	case "${option}" in
		b)
			bot_token="${OPTARG}"
			;;
		c)
			chat_id="${OPTARG}"
			;;
		v)
			usage
			;;
		h)
			usage
			;;
		*)
			usage
			;;
	esac
done

# check if all parameters are set
test -z "${chat_id}" || test -z "${bot_token}" && usage

# check if all dependencies are installed
for dependency in curl; do
	if ! which "${dependency}" &>/dev/null; then
		echo dependency \"${dependency}\" not found
		exit 1
	fi
done

# call sendMessage
sendMessage "${bot_token}" "${chat_id}" "$(IFS= cat -)"
